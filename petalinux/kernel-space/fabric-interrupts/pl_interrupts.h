#pragma once

#include <linux/interrupt.h>
#include <linux/mutex.h>
#include <linux/spinlock.h>
int pl_reset( unsigned offset,  uint8_t msec, bool sync );

struct platform_device;

struct zynq_pl_irq {
    uint8_t id;
    uint32_t counter;
    struct timespec tv;
    const char *name;
};

struct zynq_pl_device {
    struct zynq_pl_ops *ops;
    size_t noOfInterrupts;
    struct zynq_pl_irq interrupts[8];
};

struct zynq_pl_ops {
    int (*read)(struct zynq_pl_device *dev, unsigned int reg, uint32_t *val);
    int (*write)(struct zynq_pl_device *dev, unsigned int reg, uint32_t val);
    int (*setup)(struct platform_device *platform, int irq );
    irqreturn_t (*interrupt_handler)(int irq, void *devid);
	unsigned int flags;
    void __iomem *ps_gpio;
};
