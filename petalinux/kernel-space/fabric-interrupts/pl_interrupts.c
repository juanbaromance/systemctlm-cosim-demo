#include <linux/clk.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/sysfs.h>
#include <linux/of_irq.h>
#include <linux/mfd/syscon.h>
#include "pl_interrupts.h"

#ifndef nullptr
#define nullptr NULL
#endif

static irqreturn_t interrupt_handler(int irq, void *dev_id);
static struct zynq_pl_ops zynq_ops = {
    .read = nullptr,
    .write = nullptr,
    .interrupt_handler = interrupt_handler,
};

static irqreturn_t interrupt_handler( int irq, void *arg )
{
    struct platform_device *pdev = ( struct platform_device *)arg;
    struct zynq_pl_device *zpl = pdev->dev.driver_data;
    struct zynq_pl_irq *zpl_irq = zpl->interrupts +0;

    size_t i;
    for( i = 0; i < zpl->noOfInterrupts; ++i, ++zpl_irq )
    {
        if( zpl_irq->id != irq )
            continue;
        zpl_irq->tv = current_kernel_time();
        zpl_irq->counter++;
        dev_notice( & pdev->dev, "%s gpio(%2d).%s irq(%2d) : %04d\n", __PRETTY_FUNCTION__ ,
                    i, zpl_irq->name, zpl_irq->id, zpl_irq->counter );
        return IRQ_HANDLED;
    }
    return IRQ_NONE;
}

static struct of_device_id pl_compatible_dtb[] = { { .compatible = "remote-port-gpio", & zynq_ops }, { }, };
MODULE_DEVICE_TABLE(of, pl_compatible_dtb);


static struct zynq_pl_device zpl = { .ops = & zynq_ops };


static int probe( struct platform_device *pdev )
{
    const struct of_device_id *id;
    struct device* device = & pdev->dev;

    if ( ! device->of_node )
		return -ENODEV;

    dev_notice( device, " guess");
    if ( !( id = of_match_node( pl_compatible_dtb, pdev->dev.of_node ) ) )
		return -EINVAL;

    uint32_t ic = 0;
    if( of_property_read_u32( device->of_node, "interrupt-parent", & ic ) < 0 )
        return -ENODEV;

    dev_notice( device, "%10s: startup", __PRETTY_FUNCTION__ );
    platform_set_drvdata( pdev, & zpl );

    uint32_t pines;
    if( of_property_read_u32( device->of_node, "num-gpios", & pines ) < 0 )
    {
       dev_err( device, ".%s : kernel.dts miss \"num-gpios\" property\n", __PRETTY_FUNCTION__ );
       return -EINVAL;
    }

    const char *interrupts[ pines ];
    unsigned i = 0;
    for( i = 0; i < pines; i ++ )
        interrupts[ i ] = nullptr;
    of_property_read_string_helper( device->of_node, "interrupts-names", interrupts, pines, 0 );

    zpl.noOfInterrupts = 0;
    uint32_t irq_mask = 0;
    unsigned j;
    for( i = 0, j = 0; i < pines; ++i )
    {
        int ret, irq = platform_get_irq( pdev,(unsigned)i );
        if( ( ret = devm_request_irq( device, (unsigned)irq, zpl.ops->interrupt_handler, 0, pdev->name, pdev ) ) )
        {
            dev_err( device, " irq(%d) cannot register interrupt handler err=%d\n", irq, ret);
            continue;
        }
        zpl.noOfInterrupts++;
        zpl.interrupts[ j ].id = ( uint8_t )irq;
        zpl.interrupts[ j ].counter = 0;
        zpl.interrupts[ j ].name = interrupts[ i ];
        irq_mask |= 1 << i;
        dev_notice( device, "gpio(%2d).%s irq(%2d) : l o c k e d",  i, interrupts[ i ], irq );
        j++ ;
    }
    pl_reset( 0, 20, true );
    return 0;

}

static int remove( struct platform_device *pdev )
{
    struct device* device = & pdev->dev;
    dev_notice( device, "%10s: done", __PRETTY_FUNCTION__ );
    return 0;
}



static struct platform_driver zynq_pl = {
    .probe = probe,
    .remove = remove,
	.driver = {
        .name = "ps7-fabric",
        .of_match_table = pl_compatible_dtb,
	},
};

module_platform_driver(zynq_pl);
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("<usain.bolt@gmail.com>");
MODULE_DESCRIPTION("ZynQCodesign");
