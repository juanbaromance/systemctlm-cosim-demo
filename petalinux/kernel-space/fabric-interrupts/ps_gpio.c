#include <linux/device.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/sysfs.h>
#include <linux/of_irq.h>
#include "ps_gpio.h"

#ifndef nullptr
#define nullptr NULL
#endif

struct gpio_ps_t {
    const char *name;
    void __iomem *base_addr;
};

struct gpio_inspector_ops {
    int (*read  )(unsigned int reg, uint32_t *val);
    int (*write )(unsigned int reg, uint32_t val);
    int (*setup )(struct platform_device *platform, int irq );
    unsigned int flags;
};

static const struct gpio_inspector_ops ps_ops = {
    .read = nullptr,
    .write = nullptr,
};

static struct gpio_ps_t gpio_ps = { .name = "zynQ.PS.Gpio.inspector" };
static const struct of_device_id gpio_inspector_dtb[] =
{ { .compatible = "xlnx,zynq-gpio-1.0" , & ps_ops }, { }, };
MODULE_DEVICE_TABLE(of, gpio_inspector_dtb);


static int probe_gpio( struct platform_device *pdev )
{
    const struct of_device_id *id;
    struct device* device = & pdev->dev;

    dev_notice( device, " guessing");

    if ( ! device->of_node )
        return -ENODEV;

    if ( !( id = of_match_node( gpio_inspector_dtb, pdev->dev.of_node ) ) )
        return -EINVAL;

    dev_notice( device, ".%s", __PRETTY_FUNCTION__ );
    platform_set_drvdata( pdev, & gpio_ps );
    struct resource *res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
    gpio_ps.base_addr = devm_ioremap_resource(&pdev->dev, res);

    uint32_t pines;
    if( of_property_read_u32( device->of_node, "num-gpios", & pines ) < 0 )
    {
       dev_err( device, ".%s : kernel.dts miss \"num-gpios\" property\n", __PRETTY_FUNCTION__ );
       return -EINVAL;
    }
    return 0;
}

static int remove_gpio( struct platform_device *pdev )
{
    struct device* device = & pdev->dev;
    dev_notice( device, "%s", __PRETTY_FUNCTION__ );
    return 0;
}

static struct platform_driver pdriver = {
    .driver = {
        .name = "zynq-gpio-inspector",
        .of_match_table = gpio_inspector_dtb,
    },
    .probe = probe_gpio,
    .remove = remove_gpio,
};

module_platform_driver(pdriver);
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("<usain.bolt@gmail.com>");
MODULE_DESCRIPTION("ZynQ.PSGPIO.inspector");
