#include <linux/clk.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/sysfs.h>
#include <linux/of_irq.h>
#include <linux/mfd/syscon.h>
#include <linux/reset-controller.h>
#include <linux/regmap.h>
#include <linux/kthread.h>

int pl_reset( unsigned offset,  uint8_t msec, bool sync );

#ifndef KBUILD_MODNAME
#define KBUILD_MODNAME "ps7-fabric-reset"
#endif

#ifndef nullptr
#define nullptr NULL
#endif

enum
{
    ResetReady = 0,
};

struct pl_reset_data {
    struct regmap *slcr;
    struct reset_controller_dev rcdev;
    const char *name;
    u32 offset, index;
    uint8_t msec;
    bool synchronous;
    int state;
    volatile unsigned long flags;
};


#define to_zynq_reset_data(p) container_of((p), struct pl_reset_data, rcdev)

static int zynq_reset_assert( struct reset_controller_dev *rcdev, unsigned long id)
{
    struct pl_reset_data *priv = to_zynq_reset_data(rcdev);
    int bank = id / BITS_PER_LONG;
    int offset = id % BITS_PER_LONG;
    pr_debug("%s: %s reset bank %u offset %u\n", priv->name, __PRETTY_FUNCTION__, bank, offset);
    return regmap_update_bits(priv->slcr, priv->offset + (bank * 4), BIT(offset), BIT(offset));
}

static int zynq_reset_deassert( struct reset_controller_dev *rcdev, unsigned long id)
{
    struct pl_reset_data *priv = to_zynq_reset_data(rcdev);
    int bank = id / BITS_PER_LONG;
    int offset = id % BITS_PER_LONG;
    pr_debug("%s: %s reset bank %u offset %u\n", priv->name, __PRETTY_FUNCTION__, bank, offset);
    return regmap_update_bits(priv->slcr, priv->offset + (bank * 4), BIT(offset), ~BIT(offset));
}

static int zynq_reset_status( struct reset_controller_dev *rcdev, unsigned long id )
{
    struct pl_reset_data *priv = to_zynq_reset_data(rcdev);
    int bank = id / BITS_PER_LONG;
    int offset = id % BITS_PER_LONG;
    int ret;
    u32 reg;

    pr_debug("%s: %s reset bank %u offset %u\n", priv->name, __PRETTY_FUNCTION__, bank, offset);
    ret = regmap_read(priv->slcr, priv->offset + (bank * 4), &reg);
    if (ret)
        return ret;
    return !!(reg & BIT(offset));
}

static DECLARE_WAIT_QUEUE_HEAD(my_event);
static struct platform_device *MyPlatforDev;

static int deferred_reset( void *arg )
{
    struct device* device = & MyPlatforDev->dev;
    struct pl_reset_data *priv = platform_get_drvdata( MyPlatforDev );
    struct reset_controller_dev *rcdev = & priv->rcdev;

    unsigned offset = priv->index, value_of_resets;
    rcdev->ops->assert( rcdev, offset);
    msleep( priv->msec < 10 ? 10 : priv->msec );
    regmap_read( priv->slcr, priv->offset, & value_of_resets);
    dev_notice( device, "%10s: offset(%2d:%d)($%08x)", __PRETTY_FUNCTION__ , offset, zynq_reset_status( rcdev, offset ), value_of_resets );
    rcdev->ops->deassert( rcdev, offset );
    rcdev->ops->status( rcdev, offset );
    priv->state = 0;
    if( priv->synchronous )
    {
        set_bit(ResetReady, &priv->flags);
        wake_up_interruptible( & my_event );
    }
    return 0;
}

static const struct reset_control_ops zynq_reset_ops = {
    .assert		= zynq_reset_assert,
    .deassert	= zynq_reset_deassert,
    .status		= zynq_reset_status,
};


static int probe( struct platform_device *pdev )
{
    struct resource *res;
    struct pl_reset_data *priv;
    struct device* device = & pdev->dev;

    if ( ( priv = devm_kzalloc(device, sizeof(*priv), GFP_KERNEL) ) == nullptr )
        return -ENOMEM;

    platform_set_drvdata(pdev, priv);
    priv->slcr = syscon_regmap_lookup_by_phandle( device->of_node,"syscon");
    if (IS_ERR(priv->slcr))
    {
        dev_err( device, "%10s : unable to get zynq-slcr regmap", __PRETTY_FUNCTION__ );
        return PTR_ERR( priv->slcr );
    }

    if ( ( res = platform_get_resource(pdev, IORESOURCE_MEM, 0 ) ) == nullptr )
    {
        dev_err(device, "%10s: missing IO resource", __PRETTY_FUNCTION__ );
        return -ENODEV;
    }

    priv->offset = res->start;
    priv->rcdev.owner = THIS_MODULE;
    priv->rcdev.nr_resets = resource_size(res) / 4 * BITS_PER_LONG;
    priv->rcdev.ops = & zynq_reset_ops;
    priv->rcdev.of_node = device->of_node;
    priv->name = "pl-reset";
    //    int ret_val = devm_reset_controller_register( device, & priv->rcdev);
    MyPlatforDev = pdev;

    {
        unsigned tmp;
        regmap_read( priv->slcr, priv->offset, & tmp );
        dev_notice( device, "%10s: r-state $%04x:%08x : done ", __PRETTY_FUNCTION__ , priv->offset, tmp );
    }
    return 0;

}

static int remove( struct platform_device *pdev )
{
    struct device* device = & pdev->dev;
    dev_notice( device, "%10s: done ", __PRETTY_FUNCTION__ );
    return 0;
}

int pl_reset( unsigned offset,  uint8_t msec, bool sync )
{
    struct device* device = & MyPlatforDev->dev;
    struct pl_reset_data *priv = platform_get_drvdata( MyPlatforDev );

    priv->index = offset;
    priv->msec = msec;
    priv->synchronous = sync;
    priv->state = priv->flags = 0;
    kthread_run( deferred_reset, nullptr, "pl-deferred-reset" );

    if( sync )
    {
        wait_event_interruptible(my_event, test_bit(ResetReady, &priv->flags));
        clear_bit(ResetReady, & priv->flags);
    }
    dev_notice( device, "%10s: done", __PRETTY_FUNCTION__ );
    return priv->state;
}

static const struct of_device_id zynq_reset_dt_ids[] = { { .compatible = "zynqPL-reset", },  { /* sentinel */ }, };
static struct platform_driver probe_reset_driver = {
    .probe	= probe,
    .remove = remove,
    .driver = {
        .name		= KBUILD_MODNAME,
        .of_match_table	= zynq_reset_dt_ids,
    },
};

module_platform_driver(probe_reset_driver);
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("<usain.bolt@gmail.com>");
MODULE_DESCRIPTION("ZynQReset");
EXPORT_SYMBOL(pl_reset);
