/*
* peek utility - for those who remember the good old days!
*
*
* Copyright (C) 2013 - 2016  Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or (b) that interact
* with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in this
* Software without prior written authorization from Xilinx.
*
*/

#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <iostream>
#include <sstream>

void usage(char *prog)
{
    printf("usage: %s ADDR [VAL]\n",prog);
    printf("\n");
    printf("ADDR and VAL may be specified as hex values\n");
}

int main(int argc, char *argv[])
{

    if( argc < 2 ) {
        usage( argv[0] );
        exit( -1 );
    }

    int fd = open("/dev/mem",O_RDWR);
    if( fd < 1) {
        perror(argv[0]);
        exit(-1);
    }

    {

        unsigned addr=strtoul(argv[1],NULL,0);
        unsigned page_size = sysconf(_SC_PAGESIZE);
        unsigned page_addr = ( addr & ~( page_size-1));
        void *ptr = mmap( NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, ( addr & ~(page_size-1) ) );
        if( ptr == nullptr )
        {
            perror(argv[0]);
            exit(-1);
        }

        unsigned page_offset = addr - page_addr;
        unsigned & reference = reinterpret_cast<unsigned*>( ptr + page_offset )[0];
        std::ostringstream oss;

        oss << __PRETTY_FUNCTION__ << std::hex << "0x" << addr;
        if( argc == 3 )
        {
            unsigned val = strtoul( argv[2], NULL,0);
            reference = val;
            //*((unsigned *)(ptr+page_offset))=val;
            oss << " writes 0x" << val << ":" << std::dec << val;
        }
	unsigned value = reference;
	oss << " : mem states -> 0x" << value <<std::dec << ":" << value << std::endl;
        ( std::cout << oss.str() ).flush();

    }
    return 0;

}
