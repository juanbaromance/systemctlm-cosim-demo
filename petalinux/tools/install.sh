#!/bin/bash
COLOR4='\033[00;34m'
COLOR5='\[\033[00;32m\]'
BOLD='\033[00;1m'
RESET_COLOR='\033[0m'

host="192.6.1.131"
host="10.0.2.15"
remote_path="/opt/shared/bin"
target="peekpoke.x"

[ ! -e ${target} ] && target="build.zynq/peekpoke.x";

printf "\nInstalling ${BOLD}${COLOR4}${target}${RESET_COLOR} on ${host}:${remote_path}\n"
scp ${target} root@${host}:${remote_path}
