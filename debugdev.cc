/*
 * A demo/debug device.
 *
 * Copyright (c) 2013 Xilinx Inc.
 * Written by Edgar E. Iglesias
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define SC_INCLUDE_DYNAMIC_PROCESSES

#include <inttypes.h>
#include <sstream>

#include "tlm_utils/simple_initiator_socket.h"
#include "tlm_utils/simple_target_socket.h"

using namespace sc_core;
using namespace std;

#include "debugdev.h"
#include <sys/types.h>
#include <time.h>
#include <bitset>

debugdev::debugdev(sc_module_name name)	: sc_module(name), socket("debug-socket")
{
	socket.register_b_transport(this, &debugdev::b_transport);
	socket.register_transport_dbg(this, &debugdev::transport_dbg);
    ps_wires = std::make_unique<PSWires>(*this);
}

void debugdev::b_transport(tlm::tlm_generic_payload& trans, sc_time& delay)
{
	tlm::tlm_command cmd = trans.get_command();
	sc_dt::uint64 addr = trans.get_address();
	unsigned char *data = trans.get_data_ptr();
	unsigned int len = trans.get_data_length();
	unsigned char *byt = trans.get_byte_enable_ptr();
	unsigned int wid = trans.get_streaming_width();

    if (byt != 0)
    {
		trans.set_response_status(tlm::TLM_BYTE_ENABLE_ERROR_RESPONSE);
		return;
	}

	if (len > 4 || wid < len) {
		trans.set_response_status(tlm::TLM_BURST_ERROR_RESPONSE);
		return;
	}

	// Pretend this is slow!
	delay += sc_time(1, SC_US);
    if (trans.get_command() == tlm::TLM_READ_COMMAND)
       trans.set_response_status(  pull( addr, data, len, delay ) );
    else if (cmd == tlm::TLM_WRITE_COMMAND)
    {
        uint32_t & payload = * reinterpret_cast<uint32_t *>(data);
        trans.set_response_status( push( addr, payload, delay ) );
        return;
	}

	trans.set_response_status(tlm::TLM_OK_RESPONSE);
}
unsigned int debugdev::transport_dbg(tlm::tlm_generic_payload& trans)
{
	unsigned int len = trans.get_data_length();
    return len;
}
tlm::tlm_response_status debugdev::push( sc_dt::uint64 addr, const uint32_t & payload, sc_time& delay )
{
    static sc_time old_ts = SC_ZERO_TIME, now, diff;
    now = sc_time_stamp() + delay;
    diff = now - old_ts;

    switch (addr) {
    case 0:
        ( cout << __PRETTY_FUNCTION__ << ".TRACE : " << hex << payload << " " << now << " diff=" << diff << "\n" ).flush();
        break;

    case 0x4:
        putchar(payload);
        break;

    case 0x8:
        cout << __PRETTY_FUNCTION__ << ".STOP : " <<  hex << payload << " " << now << "\n";
        sc_stop();
        exit(1);

    case 0xc:
    {
        bool state = bitset<32>(payload).test(0);
        irq.write( state );
        cout << __PRETTY_FUNCTION__ << ".IRQ payloaded as 0b" <<bitset<32>(payload) << ( state ? " Toggled" : " Untoggled" ) << std::endl;
    }
        break;

    default:
        return tlm::TLM_ADDRESS_ERROR_RESPONSE;
    }

    old_ts = now;
    return tlm::TLM_OK_RESPONSE;
}
tlm::tlm_response_status debugdev::pull(sc_dt::uint64 addr, unsigned char* payload, unsigned int len, sc_time &delay)
{

    sc_time now = sc_time_stamp() + delay;
    uint32_t v = 0;
    switch (addr) {
        case 0:
            v = now.to_seconds() * 1000 * 1000 * 1000;
            break;
        case 0xc:
            v = irq.read();
            break;
        case 0x10:
            v = clock();
            break;

        default:
            return(tlm::TLM_ADDRESS_ERROR_RESPONSE);
    }

    std::cout << __PRETTY_FUNCTION__ << ".addr " << hex << addr << " handshaked with : " << dec << std::bitset<32>(v).to_ulong()
              << ":0b" << std::bitset<32>(v) << endl;
    memcpy( payload, & v, len );
    return tlm::TLM_OK_RESPONSE;
}

PSWires::PSWires(Auditor & auditor_ ): sc_module( sc_module_name("PSReset") ), auditor( auditor_ )
{
    SC_THREAD(update)
            sensitive << i_reset;
}

#include <iostream>
#include <iomanip>
void PSWires::update(void)
{
    {
        std::stringstream oss;
        oss << __PRETTY_FUNCTION__ << ": scheduled";
        auditor.report(oss.str());
    }

    while ( true )
    {
        wait();
        if( i_reset.event() )
        {
            std::stringstream oss;
            oss << name() << "." << __FUNCTION__ << " : triggered on "
                << std::setw(5) << std::boolalpha << i_reset.read() << " condition";
            auditor.report( oss.str() );
        }
    }
}


#include <sys/time.h>
void Auditor::report( const string &backtrace )
{
    std::ostringstream oss;
    time_t t; time( & t );
    struct tm * tm = localtime( & t );
    struct timeval tv;
    gettimeofday( & tv, nullptr);
    oss << setfill ('0') << setw (2) << tm->tm_hour;
    oss << ":" << setfill ('0') << setw (2) << tm->tm_min;
    oss << ":" << setfill ('0') << setw (2) << tm->tm_sec;
    oss << ":" << setfill ('0') << setw (3) << static_cast<int>( round( tv.tv_usec / 1000 )) << " ";
    ( std::cout << oss.str().c_str() << backtrace << std::endl ).flush();
}
