#!/bin/bash

simulator="/opt/petalinux/2019.2/sysroots/x86_64-petalinux-linux/usr/bin/qemu-xilinx/qemu-system-aarch64"

simulator="/home/juanba/ChallengeX/branches/zynq/qemu/xilinx.qemu/build/aarch64-softmmu/qemu-system-aarch64"
display="-serial /dev/null -serial mon:stdio -display none"
#dtb="-dtb system.dtb"
dtb="-dtb ChallengeX.QEMU.dtb"
machine="-M arm-generic-fdt-7series -machine linux=on -gdb tcp::9000 "

device1="-device loader,addr=0xf8000008,data=0xDF0D,data-len=4"
device2="-device loader,addr=0xf8000140,data=0x00500801,data-len=4"
device3="-device loader,addr=0xf800012c,data=0x1ed044d,data-len=4"
device4="-device loader,addr=0xf8000108,data=0x0001e008,data-len=4"
device5="-device loader,addr=0xF8000910,data=0xF,data-len=0x4"

# if machine PCI is provided -> edu="-device edu"
chipset_setup="$device1 $device2 $device3 $device4 $device5"

embedded="-kernel zImage"

sd="-drive file=qemu_sd.img,if=sd,format=raw,index=0 -boot mode=3"
network="-net nic -net tap,ifname=tap1"
co_design="-machine-path ./co-design.tmp"

${simulator} ${machine} ${edu} ${display} ${dtb} ${chipset_setup} ${sd} ${embedded} ${network} ${co_design}

