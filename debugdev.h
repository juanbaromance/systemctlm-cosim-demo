/*
 * Copyright (c) 2013 Xilinx Inc.
 * Written by Edgar E. Iglesias.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * JB#03/03/2020 Extension on auditory and PS.reset wire through composite pattern
 */
#pragma once
#include "tlm_utils/simple_target_socket.h"
#include <systemc.h>
#include <string>

class Auditor
{
public:
    void report( const std::string & backtrace );
};


class PSWires: public sc_module
{
public:
    SC_HAS_PROCESS(PSWires);
    PSWires( Auditor & auditor );
    sc_in<bool> i_reset{"PSResetWire"};

private:
    void update(void);
    Auditor & auditor;
};

#include <memory>
class debugdev : public sc_module, public Auditor
{
public:
    tlm_utils::simple_target_socket<debugdev> socket;
    sc_out<bool> irq;

    debugdev( sc_module_name name );
    virtual void b_transport(tlm::tlm_generic_payload& trans, sc_time& delay);
    virtual unsigned int transport_dbg(tlm::tlm_generic_payload& trans);
    std::unique_ptr<PSWires> ps_wires;

private:
    tlm::tlm_response_status push( sc_dt::uint64 addr, const uint32_t & payload, sc_time& delay );
    tlm::tlm_response_status pull( sc_dt::uint64 addr, unsigned char* payload, unsigned int len, sc_time& delay );

};


