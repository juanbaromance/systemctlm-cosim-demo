TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH = ./dependencies/include/libremote-port ./dependencies/include/qemu ./dependencies/include/
SOURCES += \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-gpio.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-memory-master.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-memory-slave.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-net.c \
	../../Qemu/Xilinx.qemu/hw/core/remote-port-pci-device.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-proto.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-qdev.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-stream.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port.c \
	../../debugdev.cc \
	../../demo-dma.cc \
	../../libsystemctlm-soc/libremote-port/remote-port-proto.c \
	../../libsystemctlm-soc/libremote-port/remote-port-sk.c \
	../../libsystemctlm-soc/libremote-port/remote-port-tlm-memory-master.cc \
	../../libsystemctlm-soc/libremote-port/remote-port-tlm-memory-slave.cc \
	../../libsystemctlm-soc/libremote-port/remote-port-tlm-wires.cc \
	../../libsystemctlm-soc/libremote-port/remote-port-tlm.cc \
	../../libsystemctlm-soc/libremote-port/safeio.c \ \
	../../memory.cc \
	../../zynqmp_demo.cc

DISTFILES +=
