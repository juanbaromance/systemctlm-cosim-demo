TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH = ./dependencies/include/libremote-port ./dependencies/include/qemu ./dependencies/include/kernel
SOURCES += \
	../../Qemu/Xilinx.Qemu/hw/core/fdt_generic_devices_cadence.c \
	../../Qemu/Xilinx.Qemu/hw/core/qdev.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-gpio.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-memory-master.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-memory-slave.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-net.c \
	../../Qemu/Xilinx.Qemu/hw/gpio/xilinx-axi-gpio.c \
	../../Qemu/Xilinx.Qemu/hw/gpio/xlnx-zynqmp-gpio.c \
	../../Qemu/Xilinx.Qemu/hw/i2c/cadence_i2c.c \
	../../Qemu/Xilinx.Qemu/hw/intc/arm_gic.c \
	../../Qemu/Xilinx.Qemu/hw/misc/xilinx-ddrc.c \
	../../Qemu/Xilinx.Qemu/hw/misc/zynq-pll.c \
	../../Qemu/Xilinx.Qemu/hw/misc/zynq-xadc.c \
	../../Qemu/Xilinx.Qemu/hw/misc/zynq_slcr.c \
	../../Qemu/Xilinx.Qemu/hw/timer/cadence_ttc.c \
	../../Qemu/Xilinx.Qemu/hw/timer/xilinx_iomod_pit.c \
	../../Qemu/Xilinx.Qemu/hw/timer/xilinx_timer.c \
	../../Qemu/Xilinx.Qemu/hw/watchdog/xlnx-swdt.c \
	../../Qemu/Xilinx.qemu/hw/core/remote-port-pci-device.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-proto.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-qdev.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port-stream.c \
	../../Qemu/Xilinx.Qemu/hw/core/remote-port.c \
	../../debugdev.cc \
	../../libsystemctlm-soc/libremote-port/remote-port-proto.c \
	../../libsystemctlm-soc/libremote-port/remote-port-sk.c \
	../../libsystemctlm-soc/libremote-port/remote-port-tlm-memory-master.cc \
	../../libsystemctlm-soc/libremote-port/remote-port-tlm-memory-slave.cc \
	../../libsystemctlm-soc/libremote-port/remote-port-tlm-wires.cc \
	../../libsystemctlm-soc/libremote-port/remote-port-tlm.cc \
	../../libsystemctlm-soc/libremote-port/safeio.c \
	../../libsystemctlm-soc/zynq/xilinx-zynq.cc \
	../../petalinux/kernel-space/fabric-interrupts/pl_interrupts.c \
	../../petalinux/kernel-space/fabric-interrupts/pl_reset.c \
	../../petalinux/kernel-space/fabric-interrupts/ps_gpio.c \
	../../zynq_demo.cc \
	dependencies/ChallengeX-kernel/arch/arm/kernel/smp_twd.c \
	dependencies/ChallengeX-kernel/arch/arm/mach-zynq/slcr.c \
	dependencies/ChallengeX-kernel/drivers/clk/zynq/clkc.c \
	dependencies/ChallengeX-kernel/drivers/clocksource/cadence_ttc_timer.c \
	dependencies/ChallengeX-kernel/drivers/gpio/gpio-xilinx.c \
	dependencies/ChallengeX-kernel/drivers/gpio/gpio-zynq.c \
	dependencies/ChallengeX-kernel/drivers/i2c/busses/i2c-cadence.c \
	dependencies/ChallengeX-kernel/drivers/iio/adc/xilinx-xadc-core.c \
	dependencies/ChallengeX-kernel/drivers/reset/core.c \
	dependencies/ChallengeX-kernel/drivers/reset/reset-zynq.c \
	dependencies/ChallengeX-kernel/drivers/rtc/rtc-ds1307.c \
	dependencies/ChallengeX-kernel/drivers/watchdog/cadence_wdt.c

DISTFILES += \
	../../petalinux/kernel-space/fabric-interrupts/Makefile

HEADERS += \
	../../petalinux/kernel-space/fabric-interrupts/pl_interrupts.h
